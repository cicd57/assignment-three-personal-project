//random change for git
const express = require('express');
const app = express();
const mysql = require('mysql');
const morgan = require('morgan');
const bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const expressValidator = require('express-validator');
const fs = require('fs');
const cors = require('cors');
const dotenv = require('dotenv');
dotenv.config();
const configuration = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
}

if (process.env.DATABASE_SOCKET) { configuration.socketPath = process.env.DATABASE_SOCKET
} else {
    configuration.host = process.env.DATABASE_HOST
}
const conn = mysql.createConnection(configuration)

// connect to database
conn.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = conn;
// bring in routes
const accomplishmentRoutes = require('./routes/accomplishment');
const authRoutes = require('./routes/auth');
const projectRoutes = require('./routes/project');
const typeRoutes = require('./routes/type');
// const recordRoutes = require('./routes/record');
// const userRoutes = require('./routes/user');
// const summaryRoutes = require('./routes/summary');

// apiDocs
/*
app.get('/api', (req, res) => {
    fs.readFile('docs/apiDocs.json', (err, data) => {
        if (err) {
            res.status(400).json({
                error: err
            });
        }
        const docs = JSON.parse(data);
        res.json(docs);
    });
});
*/
// middleware -
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressValidator());
app.use(cors({
origin: '*'
}));

 app.use('/api', accomplishmentRoutes);
 app.use('/api', authRoutes);
 app.use('/api', projectRoutes);
 app.use('/api', typeRoutes);
// app.use('/api', recordRoutes);
// app.use('/api', userRoutes);
// app.use('/api', summaryRoutes);

app.use(function(err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({ error: 'Unauthorized!' });
    }
});

const port = process.env.ENV_PORT || 8080;
app.listen(port, () => {
    console.log(`A Node Js API is listening on port: ${port}`);
});
