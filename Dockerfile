FROM node:lts-alpine

ENV DATABASE_HOST \
    DATABASE_SOCKET \
    DATABASE_USER \
    DATABASE_PASSWORD \
    DATABASE_NAME

WORKDIR /app

COPY ./package*.json ./

RUN npm install

COPY ./ .

COPY ./wait-for-it.sh wait-for-it.sh

Expose 8080

RUN chmod +x wait-for-it.sh && \
    cp .env.dist .env

CMD npm start
