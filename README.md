## 1030-individual-project

# TO RUN<br />
cd  into frontend npm run start<br />
cd  into backend npm run start<br />
dump sql.sql in mysql<br />
admin user is admin@admin.com pass 12345678<br />
regular user is jack@jack.om pass 12345678<br />

admin has access to admin functions<br />

https://github.com/karentutor/1030-individual-project
Register a user<br />
Login<br />
Routes are protected<br />
Shows or displays navs based on login status<br />
CRUD projects (portfolio) and accomplishments (resume)
Special thanks for George Lomidze and Bootstrap [various] from which the design of this website html / css was made.<br />
I have the start of a useful framework -- however far too much code repetion and requires a great deal of refactoring to remove repetition and make usable<br />

SQL LINES MAY BE FOUND AT<br />
backend/controllers/accomplishment : 29, 44, 53, 67, 116, 165,166<br />
backend/controllers/auth : 60 <br />
backend/controllers/project: 29, 43, 53, 66, 114, 163, 164<br /> 
backend/controllers/type: 13  <br />
backend/controllers/user: 12, 41, 48, 56, 126, 140<br />  

## TO PUT UP ON GCP
# TO make it work on NODE / EXPRESS
<br /><br />
In order to make it easier for our application to obtain its configurable information in different environments, it is best to use Environment variables.
In index.js, modify the following:
<br />
# Change
```
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'example_db'
})

const port = 3000
```
to be:
```
const configuration = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
}
const conn = mysql.createConnection(configuration)
const port = process.env.ENV_PORT
```

You will notice that we have removed the host property from the configuration. This is done to allow for it to be dynamically set. We are doing this since a MySQL connection really uses only one of two connection methods: TCP (ports and host), or sockets (socket path).
After the configuration object, let's add in the following if condition which will dynamically add the appropriate variable:

```
if (process.env.DATABASE_SOCKET) {
    configuration.socketPath = process.env.DATABASE_SOCKET
} else {
    configuration.host = process.env.DATABASE_HOST
}
const conn = mysql.createConnection(configuration)

```

## WORKING WITH GCP
Whether using an existing GCP project, or a new one, create a Cloud SQL instance, and a database from the GCP console UI. Please select Single zone in region section when creating.
Locally, be sure to set your SDK to your project by running: gcloud config set project PROJECT_ID where PROJECT_ID is the ID of your GCP project

<br /><br />
1.Build the image: 
```
docker build -t cloud-example .

```

<br />


2. Tag the image: 

```
docker tag cloud-example gcr.io/PROJECT_ID/cloud-example, PROJECT_ID is the ID of your GCP project.

```
3. Set your project:
 Locally, be sure to set your SDK to your project by running: `gcloud config set project PROJECT_ID` where `PROJECT_ID` is the ID of your GCP project

```
gcloud config set project PROJECT-ID 

```

<br />
Let's push the image now. First go to the GCP console, and in the search bar at the top, search for: Cloud Registry API, after selecting it, click on Enable API. i

Locally, run 

```

docker push gcr.io/PROJECT_ID/cloud-example

```

If you get an authentication error, please run: gcloud auth configure-docker


Let's deploy our app, run the following in your terminal: <br /><br />

```

gcloud run deploy cloud-example --image IMAGE_PATH --add-cloudsql-instances CLOUDSQL_CONNECTION_NAME --update-env-vars DATABASE_SOCKET="/cloudsql/CLOUDSQL_CONNECTION_NAME",DATABASE_USER="root",DATABASE_PASSWORD="YOUR DB PASSWORD",DATABASE_NAME="YOUR DB NAME",ENV_PORT=8080 --platform=managed 

```


Replace IMAGE_PATH with the GCR image path that you used to push to
Replace CLOUDSQL_CONNECTION_NAME to the Cloud SQL connection name found on the database overview panel. This is not your MySQL instance name!

Replace IMAGE PATH with 
```
gcr.io/strange-victory-317713/cloud-example 
```
Replace your DATABASE_PASSWORD with your root password
Replace your DATABASE_NAME with the name of your database
If using windows, note that you may have add --update-env-vars before each environment variable in the command rather than the comma
You will be prompted for the region, select the one that you did for your MySQL instance, e.g. northamerica-northeast1

You will be asked if you want to allow unauthenticated invocations, select y for Yes.



Once it has launched, visit the service URL provided and check its health path, e.g. using  your browser visit: URL/check. You should see Connected! if everything has worked. If you see an error, please check the logs to understand what is failing.

##IN SUMMARY

# Deploy to gcp
1. Obtain authentication credentials.

```bash
 gcloud auth application-default login
```

2.  Get gcp `PROJECT-ID`

```bash
gcloud projects list
```
3. Set project
``` bash
gcloud config set project PROJECT_ID
```

## backend

1) Build backend container image

```bash
docker build -t cloud-example .
```

2) Tag the image

```bash
docker tag cloud-example gcr.io/PROJECT_ID/cloud-example
```

3) Push the image

```
docker push gcr.io/strange-victory-317713/cloud-example
```

4) Deploy the image

```bash
gcloud run deploy backend --image gcr.io/strange-victory-317713/backend \
--add-cloudsql-instances CLOUDSQL_CONNECTION_NAME \
--update-env-vars DATABASE_SOCKET="/cloudsql/CLOUDSQL_CONNECTION_NAME" \
--update-env-vars DATABASE_USER="root" \
--update-env-vars DATABASE_PASSWORD="YOUR DB PASSWORD" \
--update-env-vars DATABASE_NAME="YOUR DB NAME" \
--update-env-vars ENV_PORT=8080 \
--platform=managed
 ```

## frontend

 1) Go to gcp and create a bucket with www.yourdomainname.com <br />
If you want a root level domain, you will need to use a load balance to get an IP and create A record
<br /><br />
2) Create two CNAME records in your account <br /><br />
Type : CNAME<br />
Name: www <br />
VALUE : c.stroage.googleapis.com
TTL: 600 s 
<br /><br />
Type: CANEM <br />
Name: _domainconnect 
VALUE: _domainconnect.gd.domaincontrol.com<br />
<br /><br />

3) Deploy to GCP -- start by making public
```bash
gsutil iam ch allUsers:objectViewer gs://www.nanoosedev.com
```

3) npm run build on your react (here used) files and cd into build folder -- then set index and copy to your new bucket 

```bash
gsutil web set -m index.html gs://www.nanoosedev.com

gsutil -m cp -r build/* gs://www.nanoosedev.com
 ```

3) Prevent caching if preferred
```bash
gsutil -m setmeta -r -h "Cache-control:no-store" gs://www.nanoosedev.com
```
