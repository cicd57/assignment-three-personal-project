# Cloud SQL and Cloud Run Connection Lab

## Overview
One of the challenges with Cloud SQL service within GCP is that it is not easy to setup TCP connections, e.g. utilize port and server address, to enable connections from an application. 

Instead, Cloud SQL offers a connection method using what is called a socket path. A socket path is a directory path that is mounted on a system, but it actually connects to another server, rather than contain files. This lab will walk you through converting a traditional Express project to connect with Cloud SQL, and what changes are needed.

## Instructions
1. In order to make it easier for our application to obtain its configurable information in different environments, it is best to use Environment variables.
2. In index.js, modify the following:
```javascript
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'user',
    password: 'password',
    database: 'example_db'
})

const port = 3000
```
to be:
```javascript
const configuration = {
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
}
const conn = mysql.createConnection(configuration)
const port = process.env.ENV_PORT
```
3. You will notice that we have removed the `host` property from the configuration. This is done to allow for it to be dynamically set. We are doing this since a MySQL connection really uses only one of two connection methods: TCP (ports and host), or sockets (socket path).
4. After the `configuration` object, let's add in the following `if condition` which will dynamically add the appropriate variable:
```javascript
if (process.env.DATABASE_SOCKET) {
    configuration.socketPath = process.env.DATABASE_SOCKET
} else {
    configuration.host = process.env.DATABASE_HOST
}
const conn = mysql.createConnection(configuration)
```
5. Whether using an existing GCP project, or a new one, create a Cloud SQL instance, and a database from the GCP console UI. Please select `Single zone` in region section when creating.

    Locally, be sure to set your SDK to your project by running: `gcloud config set project PROJECT_ID` where `PROJECT_ID` is the ID of your GCP project

gcloud config set project strange-victory-317713


6. Build the image: `docker build -t cloud-example .`

docker build -t cloud-example .


7. Tag the image: `docker tag cloud-example gcr.io/PROJECT_ID/cloud-example`, `PROJECT_ID` is the ID of your GCP project.

docker tag cloud-example gcr.io/strange-victory-317713/cloud-example

connection name : strange-victory-317713:us-central1:sql-practice

project ID :  strange-victory-317713


db user : docker 

db root : pass = mj7GqCNNEecP2ig

db password : 12345 

db name : sql_practice

image path : gcr.io/strange-victory-317713/cloud-example 

8. Let's push the image now. First go to the GCP console, and in the search bar at the top, search for: `Cloud Registry API`, after selecting it, click on `Enable API`. Locally, run `docker push gcr.io/PROJECT_ID/cloud-example`


docker push gcr.io/strange-victory-317713/cloud-example


    * If you get an authentication error, please run: `gcloud auth configure-docker`
9. Let's deploy our app, run the following in your terminal: `gcloud run deploy cloud-example --image IMAGE_PATH --add-cloudsql-instances CLOUDSQL_CONNECTION_NAME --update-env-vars DATABASE_SOCKET="/cloudsql/CLOUDSQL_CONNECTION_NAME",DATABASE_USER="root",DATABASE_PASSWORD="YOUR DB PASSWORD",DATABASE_NAME="YOUR DB NAME",ENV_PORT=8080 --platform=managed
`

gcloud run deploy cloud-example --image gcr.io/strange-victory-317713/cloud-example --add-cloudsql-instances strange-victory-317713:us-central1:sql-practice --update-env-vars DATABASE_SOCKET="/cloudsql/strange-victory-317713:us-central1:sql-practice",DATABASE_USER="root",DATABASE_PASSWORD="12345",DATABASE_NAME="sql_practice",ENV_PORT=8080 --platform=managed



    * Replace IMAGE_PATH with the GCR image path that you used to push to
    * Replace CLOUDSQL_CONNECTION_NAME to the Cloud SQL connection name found on the database overview panel. **This is not your MySQL instance name!**
    * Replace your DATABASE_PASSWORD with your root password
    * Replace your DATABASE_NAME with the name of your database
    * If using windows, note that you may have add `--update-env-vars` before each environment variable in the command rather than the comma
    * You will be prompted for the region, select the one that you did for your MySQL instance, e.g. `northamerica-northeast1`
    * You will be asked if you want to allow unauthenticated invocations, select `y` for Yes.
10. Once it has launched, visit the service URL provided and check its health path, e.g. using  your browser visit: `URL/check`. You should see `Connected!` if everything has worked. If you see an error, please check the logs to understand what is failing.

